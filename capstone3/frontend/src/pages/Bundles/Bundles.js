import React from 'react'

import { Bundle } from '../../components'
import './Bundles.scss'
import imgBanner from '../../assets/templates-banner.png'
import img1 from '../../assets/product-1.png'


const bundleData = [
  {
    name: 'Bundle #1',
    description: `Lorem ipsum dolor sit amet. Sed enim provident aut eaque alias ab iste possimus est voluptas reprehenderit non voluptas dolor. Quo ipsum facere est necessitatibus dolores id explicabo....\

    Lorem ipsum dolor sit amet. Sed enim provident aut eaque alias ab iste possimus est voluptas reprehenderit non voluptas dolor. Quo ipsum facere est necessitatibus dolores id explicabo....
    
    Lorem ipsum dolor sit amet. Sed enim provident aut eaque alias ab iste possimus est voluptas reprehenderit non voluptas dolor. Quo ipsum facere est necessitatibus dolores id explicabo....
    
    Lorem ipsum dolor sit amet. Sed enim provident aut eaque alias ab iste possimus est voluptas reprehenderit non voluptas dolor. Quo ipsum facere est necessitatibus dolores id explicabo....`,
    price: 2000,
    image: img1
  },
  {
    name: 'Bundle #2',
    description: `Lorem ipsum dolor sit amet. Sed enim provident aut eaque alias ab iste possimus est voluptas reprehenderit non voluptas dolor. Quo ipsum facere est necessitatibus dolores id explicabo....

    Lorem ipsum dolor sit amet. Sed enim provident aut eaque alias ab iste possimus est voluptas reprehenderit non voluptas dolor. Quo ipsum facere est necessitatibus dolores id explicabo....
    
    Lorem ipsum dolor sit amet. Sed enim provident aut eaque alias ab iste possimus est voluptas reprehenderit non voluptas dolor. Quo ipsum facere est necessitatibus dolores id explicabo....
    
    Lorem ipsum dolor sit amet. Sed enim provident aut eaque alias ab iste possimus est voluptas reprehenderit non voluptas dolor. Quo ipsum facere est necessitatibus dolores id explicabo....`,
    price: 2000,
    image: img1
  },
  {
    name: 'Bundle #3',
    description: `Lorem ipsum dolor sit amet. Sed enim provident aut eaque alias ab iste possimus est voluptas reprehenderit non voluptas dolor. Quo ipsum facere est necessitatibus dolores id explicabo....\

    Lorem ipsum dolor sit amet. Sed enim provident aut eaque alias ab iste possimus est voluptas reprehenderit non voluptas dolor. Quo ipsum facere est necessitatibus dolores id explicabo....
    
    Lorem ipsum dolor sit amet. Sed enim provident aut eaque alias ab iste possimus est voluptas reprehenderit non voluptas dolor. Quo ipsum facere est necessitatibus dolores id explicabo....
    
    Lorem ipsum dolor sit amet. Sed enim provident aut eaque alias ab iste possimus est voluptas reprehenderit non voluptas dolor. Quo ipsum facere est necessitatibus dolores id explicabo....`,
    price: 2000,
    image: img1
  },
]

const Bundles = () => {
  const viewDetails = () => {

  }

  return (
    <div className='app__bundles section__padding '>
      <div className='app__bundles-banner'>
        <img src={imgBanner} alt='banner' />
      </div>

      <div className='app__bundles-list'>
        {bundleData.map((bundle,index) => (
          <Bundle key={bundle + index} bundleProps ={bundle} onAdd = {() => (viewDetails)} />
          ))}
      </div>
    </div>
  )
}

export default Bundles
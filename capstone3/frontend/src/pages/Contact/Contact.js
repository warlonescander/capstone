import React from 'react'

import './Contact.scss'

const Contact = () => {

  const sendMessage = () => {

  }

  return (
    <div className='app__contact section__padding page__margin'>
      <h1 className='page__title'>Contact us</h1>
      <div className='app__contact-content'>
        <div className='app__contact-content_info'>
          <div>
            <h4>Customer Support hours</h4>
            <p>Monday - Saturday 8:00AM - 5:00 PM</p>
            <p>Holidays - Closed</p>
          </div>
          <p>support@weedigitals.com</p>
          <p>(+63) 956-935-2427</p>
          <p>Please, make sure you read the FAQ's here before contacting us. Your inquiry may be answered here.</p>
          <p>Our response time is within 24 business hours. Please, be patient, we will get back to you!</p>
          <div className='app__contact-content_faq'>
            <a>{`> Why I haven’t receive my template?`}</a>
            <a>{`> Do you have instructions on how to use the template?`}</a>
          </div>
        </div>

        <div className='app__contact-content_form'>
          <form onSubmit={sendMessage}>
            <div>
              <label htmlFor='fullName'>Full Name</label>
              <input id='fullName' className='custom__input' type='text' placeholder='Enter your first and last name' />
            </div>
            <div>
              <label htmlFor='email'>Email</label>
              <input id='email' className='custom__input' type='email' placeholder='Enter your email'  />
            </div>
            <div>
              <label htmlFor='about'>What is your message about?</label>
              <select id='about' className='custom__input'>
                <option>Problems downloading my file</option>
                <option>Feedback in using template</option>
                <option>New template suggestion</option>
              </select>
            </div>
            <div>
              <label htmlFor='message'>Message</label>
              <textarea id='message' className='custom__input' rows={10} placeholder='Enter your message'/>
            </div>
            <submit  className='custom__button' type='submit'>SUBMIT</submit>
          </form>
        </div>
      </div>

    </div>
  )
}

export default Contact
import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import { Card } from '../../components';
import './Templates.scss';
import banner from '../../assets/templates-banner.png';

const categories = ['All', 'Finance', 'Personal Development']

const Templates = () => {
  const [activeProducts, setActiveProducts] = useState([])

  const navigate = useNavigate()

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/active`)
    .then(results => results.json())
    .then(data => {
      setActiveProducts(data);
    })
  })

  const viewDetails = (index) => {
    // navigate(`/templates/${index}`)
  }
  
  const filterItems = () => {

  }

  return (
    <div className='app__templates section__padding'>
      <div className='app__templates-banner'>
        <img src={banner} alt='banner'/>
      </div>
      <div className='app__templates-category'>
         {categories.map((category,index) => (
          <button 
            type='button' 
            key={category + index}
            onClick={filterItems}
          >
            {category}
          </button>
         ))} 
      </div>

      <div className='app__templates-list'>
          {activeProducts.map((product, index) => (
            <Card cardProps ={product} key={product+index} onClick={() => viewDetails(product._id)} />
          ))}
      </div>
    </div>
  )
}

export default Templates
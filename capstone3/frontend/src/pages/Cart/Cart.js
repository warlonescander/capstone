import { useState } from 'react';
import { Link } from 'react-router-dom';

import { CartItem } from '../../components';
import './Cart.scss';
import img1 from '../../assets/product-1.png';
import img2 from '../../assets/product-2.png';
import img3 from '../../assets/product-3.png';

const cartData = [
  {
    name:'Product #1',
    image: img1,
    price: 1700,
    quantity: 1,
    subtotal: 1700
  },
  {
    name:'Product #2',
    image: img2,
    price: 1500,
    quantity: 1,
    subtotal: 1500
  },
  {
    name:'Product #3',
    image: img3,
    price: 1200,
    quantity: 1,
    subtotal: 1200
  }
];


const Cart = () => {
  const [totalAmount, setTotalAmount] = useState(0)
  
  return (
    <div className='app__cart section__padding page__margin'>
        <div className='app__cart-header'>
            <h1 className='page__title'>Your Cart</h1>
            <Link to={'/templates'}>Continue shopping</Link>
        </div>

        <div className='app__cart-table'>
          <table>

            <thead>
              <tr>
                <th></th>
                <th>PRODUCT</th>
                <th>PRICE</th>
                <th>QUANTITY</th>
                <th>TOTAL</th>
              </tr>
            </thead>

            <tbody>
              { cartData.map((item,index) => (
                <CartItem key={item+index} itemProps ={item}/>
              ))}
            </tbody>

          </table>
        </div>

        <div className='app__cart-footer'>
          <div className='app__cart-footer_total'>
            <h4>Total Amount Due</h4>
            <p>{totalAmount}</p>
          </div>

          <button className='custom__button'>PROCEED TO CHECK OUT</button>
        </div>
    </div>
  )
}

export default Cart
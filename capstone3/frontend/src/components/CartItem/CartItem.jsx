import { useEffect, useState } from 'react';

import './CartItem.scss'

const CartItem = (props) => {
    const { name, image, price, quantity } = props.itemProps;
    const [subtotal, setSubtotal] = useState(0);
    const [newQuantity, setNewQuantity] = useState(1);

    useEffect(() => {
        setSubtotal(newQuantity*price)
    },[newQuantity])

    useEffect(() => {
        setNewQuantity(quantity)
    },[price,quantity])

  return (
    <tr>
        <td><input type='checkbox' /></td>
        <td>
            <div>
                <img src={image} alt={name} />
                <h2>{name}</h2>
            </div>
        </td>
        <td>{price}</td>
        <td>
            <input type='number' value = {newQuantity} onChange={e => setNewQuantity(e.target.value)} />
        </td>
        <td>{subtotal}</td>

    </tr>
  )
}

export default CartItem
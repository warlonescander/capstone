import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';

import './Loader.scss'

const Loader = ({path, title, text}) => {
    const [dotCount, setDotCount] = useState(1)
    const [textWithDot, setTextWithDot] = useState(text)


    const navigate = useNavigate();

    const appendDot = (text, dots) => {
        for(let i = 0; i <= dots-1; i++) {
            text += '.'
         }
        setTextWithDot(text)
    }

    const loadTime= (path) => {
        if(path ==='/login') {
            return 5000;
        } else if (path === '/') {
            return 2000;
        } else {
            return 1000;
        }
    }

    useEffect(() => {
        setTimeout(() => {
            console.log(path)
            navigate(path)
        }, loadTime(path));
    },[])

    useEffect(() => {
        setTimeout(() => {
            appendDot(text, dotCount)
            if(dotCount==3) {
                setDotCount(0)
            } else {
                setDotCount(dotCount+1)
            }
        }, 500);
    })

    return (
        <div className='app__loader'>
            <h1>{title}</h1>
            <div class="loadingio-spinner-interwind-9djpe29nrx">
                <div class="ldio-ebbizn9h2zc">
                    <div>
                        <div>
                            <div>
                                <div></div>
                            </div>
                        </div>
                        <div>
                            <div>
                                <div></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <p>{textWithDot}</p>
        </div>
    )
}

export default Loader
import React from 'react'

import './Bundle.scss'

const Bundle = ({onAdd,...props}) => {
  const { name, description, price, image } = props.bundleProps;

  return (
    <div className='app__bundle'>
      <div className='app__bundle-images'>
        <img src={image} alt={name} />
        <div>
          <img src={image} alt={name} />
          <img src={image} alt={name} />
          <img src={image} alt={name} />
        </div>
      </div>
      <div className='app__bundle-content'>
        <div className='app__bundle-content_info'>
          <h1>{name}</h1>
          <p>{description}</p>
          <div>
            <h2>{price}</h2>
            <h3>{price*2}</h3>
          </div>
        </div>

        <button className='custom__button' onClick={onAdd}>ADD TO CART</button>
      </div>
    </div>
  )
}

export default Bundle